/**
 * HTTP router. 
 * Make use of exports and try to reuse as 
 * much code as possible.
 */

const fs = require('fs');

// Ideally make use of the route here and allow only calls within 
// the api/<something> path.
const API_PREFIX = 'api/';

// Build a route from a passed URl, return callback.
exports.getRoute = function getRoute(req, res) {
    return (function (...args) { 
        console.log(req.method, req.url, 'route called with args', args); 
        this.end(); 
    }).bind(res);
};

// Http.ServerResponse is the class that should passed into the route.
// @this = res
exports.getAll = function getAll() {
    this.end();
};

// Add an error handler if route is not found.
// ...
