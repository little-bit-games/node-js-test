# NodeJS test task

This is the test task for the NodeJS developer position. Layout of the task is already pre-defined,
you are expected to work with the files in this repo. Please, don't do a public fork so other 
competitors don't see your solution. Private git clone or a zip archive is the preferred way of working
on this task.

This task is a not a real-world application, and is intended to test your skills in NodeJS, hence few 
restrictions are applied:

- You cannot use npm/yarn dependencies for this project
- Layout is already defined, hence you are expected to use existing files/libs, more can be added if needed
- Be it Async or Sync is up to you, there is no limitation on the features of the language you can use

## Task description

Pretty straightforward, make a CRUD server which handles two routes: 

- `/users`
- `/documents`

Both routes should respect RESTfull API principles, meaning that each HTTP method is expected to 'mean' specific thing:

- `GET /users` - get list of all users AND their documents' ids
- `GET /users/id` - get user AND his documents' ids
- `POST /users` - add a new user
- `PUT /users/id` - change user data
- `DELETE /users/id` - delete user record

The very same logic applies to another table 'documents'.
The structure of the tables is the following:

```
User: id int, name string
Documents: id int, user_id int, content buffer
```

As you probably see by the names of the columns of fields, `user_id` links to `user`.

## Summary

Second task is described in the FRONTEND.md file.

Send results to the email `ekaterina.h@littlebit.games` and wait for the app to be approved.
