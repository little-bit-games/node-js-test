/**
 * Index file. The server itself.
 */

const http   = require('http');
const routes = require('./lib/routes');

const server = http.createServer(function (req, res) {
    routes.getRoute(req, res)();
});

server.listen(3000);
