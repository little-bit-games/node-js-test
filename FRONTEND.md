# Frontend test task

As an addition to the NodeJS task, you are expected to build a Single Page Application,
which would have pages respective to the API routes. Imagine it is an admin panel
where you have the permissions to view/edit/delete the records. 

## Login extension

The improved task is to add a login page which will allow admins to login and perform
changes over the data, others (non-logged in) users would also be able to view the data.

The design of everything is completely up to you, pick any framework / tooling you want 
for the frontend. You can do a separate web server to serve the frontend as well.
